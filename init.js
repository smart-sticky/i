/*------------------------------------------------
    
    smartSticky: sticky sidebar plugin
                 with long/tall sidebar support
	
    - written by HT (@glenthemes)
	
    gitlab.com/smart-sticky/i
    
------------------------------------------------*/

window.smartSticky = function(params){
    let smartStickyInit = (params) => {
        let elem = params.stick.trim().replaceAll(", ",",");
        let scont = params.scroll_container.trim();
        elem = document.querySelectorAll(elem);
        scont = document.querySelector(scont);
        
        // sidebar each
        elem && scont ? elem.forEach(stickyTarget => {
            let stickyHeight;
            let winHeight;
            
            function reHeight(){
                stickyHeight = stickyTarget.scrollHeight;
                stickyTarget.style.setProperty("--Target-Height",stickyHeight + "px");

                winHeight = scont.clientHeight;
            }
            
            reHeight();
            
            window.addEventListener("resize", () => {
                reHeight()
            })

            setTimeout(() => {
                reHeight();
            },2500)

            // console.log("SIDEBAR HEIGHT: " + stickyHeight)
            // console.log("WIN HEIGHT: " + winHeight)
            
            let viewportHeight = document.querySelector("html").clientHeight;
            if(stickyHeight < winHeight && stickyHeight <= viewportHeight){
                stickyTarget.classList.add("short-stick")
            } else {
                window.addEventListener("scroll", () => {
                    let scrolled = scont.scrollTop;
                    if(scrolled + scont.clientHeight >= stickyHeight+0){
                        stickyTarget.classList.add("stick")
                    } else {
                        stickyTarget.classList.remove("stick")
                    }
                }) // end scroll
            }//end if-else
        }) : "" //end sticky element each
    }//end smartStickyInit

    document.readyState == "loading" ?
    document.addEventListener("DOMContentLoaded", () => smartStickyInit(params)) :
    smartStickyInit(params);
}