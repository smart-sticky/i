### smartSticky() plugin

**Description:** jQuery-free plugin that helps stick sidebars to the top, regardless of their height.  
**Author:** HT (@&#x200A;glenthemes)

#### Demo:
:beer:&ensp;[smart-sticky.gitlab.io/i/demo](https://smart-sticky.gitlab.io/i/demo)

![Screenshot preview GIF of smart sticky plugin](https://64.media.tumblr.com/0b4a257eb8590c12c28d69b2675c383f/2b12221444e05b14-b5/s540x810/94e29395b7f7c12b3a55a151cb666de960dd8b88.gif)

#### Demo code:
:beers:&ensp;[jsfiddle.net/glenthemes/p78q40nk](https://jsfiddle.net/glenthemes/p78q40nk)  
:beers:&ensp;[jsfiddle.net/glenthemes/9L4uk16w](https://jsfiddle.net/glenthemes/9L4uk16w) (simple)

#### Requirements:
* a sticky element (e.g. a sidebar); you can have multiple.
* main content that isn't meant to be sticky.

`smartSticky()` has only been tested on a flexbox structure, such as:
```html
<main class="flex-container">
    <aside class="left-sidebar"></aside>
    <article class="main-content"></article>
    <aside class="right-sidebar"></aside>
</main>
```
```css
.flex-container {
    display:flex;
    flex-wrap:nowrap;
    align-items:flex-start;
}

.left-sidebar,
.right-sidebar {
    width:250px; /* or however wide you want the sticky element(s) to be */
}

.main-content {
    flex:1;
}
```
* Avoid including `position:absolute;` and `position:fixed;` when using `smartSticky()`.
* Your **sticky element** and **main content** should be **wrapped in a parent div/container.**
* The parent **div/container** should **not** have `height:100%;` or `height:0;`.

---

#### Step 1:
Include the following script after `<head>`:
```html
<!--✻✻✻✻✻✻  SMART STICKY by @glenthemes  ✻✻✻✻✻✻-->
<!--------   gitlab.com/smart-sticky/i  --------->
<script src="https://smart-sticky.gitlab.io/i/init.js"></script>
```

#### Step 2:
Run and configure (inside a `<script>`):
```javascript
smartSticky({
    scroll_container: "body",
    stick: ".sidebar"
})
```

* `scroll_container`: this will usually just be `body`. However, if your parent div/container is a separate scrollable element, change it to said selector.
* `stick`: the selector(s) of your sticky element(s). If you need to include multiple, the line would look something like `stick: ".sidebar, .contact-form"`.

#### Step 3:
Add this CSS (inside a `<style>`):
```css
.sidebar {
    position:sticky;
}

.sidebar.stick {
    /* you can change 40px */
    /* just don't edit the rest of the equation */
    /* 40px is the padding underneath the sidebar when it reaches the bottom */
    /* "Target-Height" is something the script will take care for you */
    top:calc(100vh - var(--Target-Height) - 40px);
}

.sidebar.short-stick {
    position:sticky;
    top:40px; /* change 40px to the "stuck" gap amount from the top of the screen. if you have a header, include the header height in the total. */
}
```
Remember to change `.sidebar` to the selector name of your sticky element. If you have multiple sticky elements with different names, you can modify it like so, e.g.:
```css
.sidebar1, .sidebar2 {
    position:sticky;
}

.sidebar1.stick, .sidebar2.stick { ... }
.sidebar1.short-stick, .sidebar2.short-stick { ... }
```

---

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)

---

#### Found this helpful?
Please consider leaving me a tip :revolving_hearts:  
&mdash;&thinsp;&hairsp;[https://ko-fi.com/glenthemes](ko-fi.com/glenthemes)
